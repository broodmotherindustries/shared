import os
from .operating_systems.raspberry_pi import RaspberryPi
from .EventHook import EventHook
import importlib.util
import sys
import psutil

if importlib.util.find_spec('RPi') != None:
    found = True
    import RPi
else:
    found = False
    import fake_rpi
    sys.modules['RPi'] = fake_rpi.RPi  # Fake RPi (GPIO)
    sys.modules['SMBUS'] = fake_rpi.smbus  # Fake SMBUS (I2C)



class SystemUtls:

    def __init__(self):
        self.pi = RaspberryPi()
        self.voltagePin = 35
        self.voltageChange = EventHook()

    def getRAMinfo(self):
        if self.pi.is_pi():
            p = os.popen('free')
            i = 0
            while 1:
                i = i + 1
                line = p.readline()
                if i == 2:
                    return (line.split()[1:4])
        else:
            return psutil.virtual_memory().percent

    def getCPUuse(self):
        if self.pi.is_pi():
            return (str(os.popen("top -n1 | awk '/Cpu\(s\):/ {print $2}'").readline().strip( \
            )))
        else:
            return psutil.cpu_percent()

    def getCPUtemperature(self):
        if self.pi.is_pi():
            res = os.popen('vcgencmd measure_temp').readline()
            return (res.replace("temp=", "").replace("'C\n", ""))
        else:
            print ('This system does not appear to be a ' 'Raspberry Pi.')

    # Return information about disk space as a list (unit included)
    # Index 0: total disk space
    # Index 1: used disk space
    # Index 2: remaining disk space
    # Index 3: percentage of disk used
    def getDiskSpace(self):
        if self.pi.is_pi():
            p = os.popen("df -h /")
            i = 0
            while 1:
                i = i + 1
                line = p.readline()
                if i == 2:
                    return (line.split()[1:5])
        else:
            response = ''
            for part in psutil.disk_partitions():
                response += (part.device + " " + str(psutil.disk_usage(part.device).percent) + "%")

            return response

    def listenForLowVoltage(self):
        if self.pi.is_pi():
            RPi.GPIO.setmode(RPi.GPIO.BCM)
            RPi.GPIO.setup(self.voltagePin, RPi.GPIO.IN, pull_up_down=RPi.GPIO.PUD_DOWN)

            RPi.GPIO.add_event_detect(self.voltagePin, RPi.GPIO.RISING, callback=self.callback, bouncetime=3000)
        else:
            print ('This system does not appear to be a ' 'Raspberry Pi.')

    def callback(self, channel):
        self.voltageChange.fire(RPi.GPIO.input(self.voltagePin))


