"""
Checks if config.Prod.yaml exists and uses that
else it uses config.Dev.yaml and returns
websokket_url and websocket_port
"""
from pathlib import Path
import ruamel.yaml as yaml


class Config:
    """
    Checks if config.Prod.yaml exists and uses that
    else it uses config.Dev.yaml and returns
    websokket_url and websocket_port
    """
    def __init__(self):
        my_file = Path("config.Prod.yaml")
        if my_file.is_file():
            file_to_use = "config.prod.yaml"
        else:
            file_to_use = "config.dev.yaml"
        # if the Prod file exists use that else dev
        with open(file_to_use) as stream:
            try:
                config = yaml.safe_load(stream)
                self.websokket_url = config['WebSocket']['URL']
                self.websocket_port = config['WebSocket']['Port']
            except yaml.YAMLError as exc:
                print(exc)
