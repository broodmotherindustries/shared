from Shared import EventHook
from unittest.case import TestCase
import unittest, inspect
from io import StringIO

from pprint import pprint
import imp
import os
import logging
import collections

# stream = StringIO()
# runner = unittest.TextTestRunner(stream=stream)
# result = runner.run(unittest.makeSuite(TestMain))
# print ('Tests run ', result.testsRun)
# print ('Errors ', result.errors)
# pprint(result.failures)
# stream.seek(0)
# print ('Test output\n', stream.read())

logging.basicConfig(level=logging.DEBUG)


class Diagnostic:

    def __init__(self, test_folder, main_module='__init__', log=logging):
        self.logging=log
        self.test_folder = test_folder
        self.main_module = main_module
        self.loaded_tests = collections.OrderedDict({})
        pass

    def run_all_unit_tests(self):

        totalTest = 0
        totalFailures = []
        totalError = 0

        stream = StringIO()
        runner = unittest.TextTestRunner(stream=stream)

        for key, test_info in self.loaded_tests.items():
            module = test_info['module']
            members = inspect.getmembers(module, inspect.isclass)
            for member in members:
                result = runner.run(unittest.makeSuite(member[1]))
                totalTest = totalTest + result.testsRun
                totalFailures.append(result.failures)

        returnText = '''        Tests Ran {0}
        Failures {1}'''.format(totalTest, totalFailures)

        return returnText

    def get_available_tests(self):
        tests = {}
        for possible in os.listdir(self.test_folder):
            location = os.path.join(self.test_folder, possible)
            if os.path.isdir(location) and self.main_module + '.py' in os.listdir(location):
                info = imp.find_module(self.main_module, [location])
                tests[possible] = {
                    'name':possible,
                    'info': info
                }
        return tests

    def load_all_test(self):
        tests = self.get_available_tests()
        for test in tests:
            if test not in self.loaded_tests:
                module = imp.load_module(self.main_module, *tests[test]['info'])
                self.loaded_tests[test] = {
                    'name': test,
                    'info': tests[test]['info'],
                    'module': module
                }
            else:
                self.logging.warn('plugin "%s" already loaded' % test)