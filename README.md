#  Shared

Shared is a collection of general Python libraries that is used across mutiple projects.

## EventHook
This library replicates the c# event manager.

## Plugin Manager 
This library loads and manages the plugin frame plugins are dynamically loaded from a supplied directory.

The plugins must be python packages and with in the __init__.py the function action_task must exist/
 
## Config Manager

Loads Yaml files based on the environment passed to it.

##Diagnostic

This library will run the unit tests  and return the results as a string

##System Utils

This library lets get information about the hardware the application is running on, currently suports 
* get RAM
* get CPU Usa
* get Disk space

the following to function are only avalible on a raspberry pi 
 * get CPU Temperature
 * listenForLowVoltage

##Operating systems

###Raspberry PI
Detects is the application is running on a reaspberry pi or not.
