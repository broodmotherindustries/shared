"""
Setup for Shared
"""
from setuptools import setup, find_packages

setup(name='shared',
      version='0.0.3',
      description='Bot BotCore',
      url='',
      author='Code Iain',
      packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
      install_requires=['ruamel.yaml'],
      zip_safe=False)
